<?php

function printThat($v) {
	echo "<pre>";
	print_r($v);
	echo "</pre>";
}
?>

<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>5 ways</title>  
	<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	
	<script type="text/javascript" url="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
	<link rel="stylesheet" href="style.css" /> 
</head>
<body>



<div class="container">
	
	<div class="lcol column">
		
		<div id="header">
			<h1>
				<img class="big-number" src="images/big5.png" alt="" />
				<strong>MISTAKES</strong> TO AVOID IN<br/>
				YOUR <strong>FRYING PROGRAM,</strong><br/>
				<span class="offset">BACKED BY 60 YEARS</span><br/>
				<span class="offset">OF FRYING EXPERTISE.</span>
			</h1>
		</div>
		
			<div class="content">

				<p>Today’s operators know that frying can have a significant impact on the bottom line, making it more important than ever to leverage the investment in a positive way — improving food quality, enhancing guests’ experiences and boosting overall profits.</p>
				<p>
					Over the years, our team of food scientists, corporate chefs, engineers and training personnel developed a process for assessing kitchen performance. After visiting countless kitchens, from small independent operations to large chains, we’ve identified several common mistakes related to frying —and now we’re bringing these valuable findings directly to you.</p>




				</div>
		</div>

		<div class="rcol column">

			<div class="content">

				<p>
					Discover <strong class="red">THE MOST COMMON FRYING MISTAKES</strong> and what you can do to make your program more profitable.
				</p>

				<p class="smaller">Download your <span class="red">FREE COPY</span> of this EXCLUSIVE ARTICLE from Henny Penny, plus a BONUS frying operational assessment to 
					identify opportunities to improve and save within your operation. 

				</p>


				<form action="http://cl.s10.exct.net/DEManager.aspx" name="subscribeForm" method="post">

					<input type="hidden" name="_clientID" value="100002106" />
					<input type="hidden" name="_deExternalKey" value="57B03E6D-8717-4FF1-BCEB-07D4807694CD" />
					<input type="hidden" name="_action" value="add" />
					<input type="hidden" name="_returnXML" value="0" />
					<input type="hidden" name="_successURL" value="http://staging.vantagep.com/hennypenny/ms_6mistakes/exact_target/?s=Success" />
					<input type="hidden" name="_errorURL" value="http://staging.vantagep.com/hennypenny/ms_6mistakes/exact_target/?s=Failure" />


					
					<ul>
						<li><label>Full Name:</label><input type="text" name="Name"></li>
						<li><label>Title:</label><input type="text" name="Title"></li>
						<li><label>Company:</label><input type="text" name="Company"></li>
						<li><label>Country:</label><input type="text" name="Country"></li>
						<li><label>State:</label><input type="text" name="State"> <label>ZIP:</label> <input type="text" name="ZIP"></li>
						<li><label>Email:</label><input type="text" name="EmailAddress"></li>
					</ul>
			
					<input type="submit" value="Submit"/>
				</form>

			</div>



		</div>

</div>

		<div id="footer" class="container-fluid">
			<div class="container">
				<img class="pull-left mobile-center" src="images/logo-hp.png" alt="HP" />
				<div class="clearfix"></div>
			</div>
		</div>

	</body>
	</html>