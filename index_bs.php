<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>5 ways</title>  
	<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
	<link rel="stylesheet" href="http://staging.vantagep.com/hennypenny/ms_5mistakes/exact_target/style_bs.css" />
	<!--<link rel="stylesheet" href="%%=CloudPagesURL(35)=%%" />-->

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script type="text/javascript">
	$=jQuery;
	
	function getURLvalue(name) {
		name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
		var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
		var results = regex.exec(location.search);
		return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	};
	
		$(document).ready(function(){

		console.log(window.location.search);

		$('.big-number').addClass('show');

		$.ajax({ 
			type: 'GET', 
			url: 'http://staging.vantagep.com/hennypenny/ms_5mistakes/exact_target/json/countries.json', //%%=CloudPagesURL(49)=%%
			dataType: 'json',
			success: function (data) { 
				$.each(data, function(index, element) {
					$('#select_country').append($('<option>', {
						value: element.name,
						text: element.name
						}));
					});
				}
			});

			$.ajax({ 
				type: 'GET', 
				url: 'http://staging.vantagep.com/hennypenny/ms_5mistakes/exact_target/json/states.json', //%%=CloudPagesURL(50)=%%
				dataType: 'json',
				success: function (data) { 
					$.each(data, function(index, element) {
						$('#select_state').append($('<option>', {
							value: element,
							text: element
							}));
						});
					}
				});

				switch(getURLvalue('s')){
					case 'Success':
					case 'Failure':
					
					$('#wrap').addClass('hide');
					$('#confirmation').removeClass('hide');
					
					
					break;
					case 'Failure':
					break;
				}


			});

			</script>

		</head>
		<body>


			<div class="container hide" id="confirmation">
				<div class="confirmation-box">
					<h2 class="red">You're all set!</h2>
					<p>Just click the download button below and you can take advantage of decades of Henny Penny frying expertise — all distilled into one convenient, informative article, plus a Frying Program Analysis worksheet.</p>
					
					<a href="#" target="_blank" class="submit-button">Download</a>
				</div>
			</div>


			<div class="container">

				<div class="row"  id="wrap">
					<div class="col-md-7 lcol">

						<div id="header">
							<h1>
								<img class="big-number" src="http://staging.vantagep.com/hennypenny/ms_5mistakes/exact_target/images/big5.png" alt="" />
								<strong>MISTAKES</strong> TO AVOID IN<br/>
								YOUR <strong>FRYING PROGRAM,</strong><br/>
								<span class="offset">BACKED BY 60 YEARS</span><br/>
								<span class="offset">OF FRYING EXPERTISE.</span>
							</h1>
						</div>

						<div class="content">

							<p>
								Today’s operators know that frying can have a significant impact on the bottom line, making it more 
								important than ever to leverage the investment in a positive way — improving food quality, enhancing guests’ experiences and boosting overall profits.
							</p>
							<p>
								Over the years, our team of food scientists, corporate chefs, engineers and training personnel developed 
								a process for assessing kitchen performance. After visiting countless kitchens, from small independent 
								operations to large chains, we’ve identified several common mistakes related to frying —and now we’re 
								bringing these valuable findings directly to you.
							</p>
						</div>

					</div>
					<div class="col-md-5 rcol">


						<div class="content">

							<p>
								Discover <strong class="red">THE MOST COMMON FRYING MISTAKES</strong> and what you can do to make your program more profitable.
							</p>

							<p class="smaller">Download your <span class="red">FREE COPY</span> of this EXCLUSIVE ARTICLE from Henny Penny, plus a BONUS frying operational assessment to 
								identify opportunities to improve and save within your operation. 

							</p>


							<div class="error hide">
								<h3>We've encountered an error.</h3>
								<p>
									<strong>It is likely that your email has already been registered.</strong><br/>  
									Please try again with a different email address.
									If there is still a problem, please try again later.
									</p>
							</div>

							<form action="http://cl.s10.exct.net/DEManager.aspx" name="subscribeForm" method="post">

								<input type="hidden" name="_clientID" value="100002755" />
								<input type="hidden" name="_deExternalKey" value="3D727419-ABB0-45D7-9FEA-D631B4336A72" />
								<input type="hidden" name="_action" value="add" />
								<input type="hidden" name="_returnXML" value="0" />
								<input type="hidden" name="_successURL" value="http://staging.vantagep.com/hennypenny/ms_6mistakes/exact_target/index_bs.php?s=Success" />
								<input type="hidden" name="_errorURL" value="http://staging.vantagep.com/hennypenny/ms_6mistakes/exact_target/index_bs.php?s=Failure" />


								<div class="row">
									<div class="col-md-6">
										<label>Full Name*:</label><input type="text" name="Name" required>
									</div>
									<div class="col-md-6">
										<label>Title*:</label><input type="text" name="Title" required>								
									</div>
									<div class="col-md-6">
										<label>Email*:</label><input type="text" name="EmailAddress" required>
									</div>

									<div class="col-md-6">
										<label>Company*:</label><input type="text" name="Company" required>
									</div>

							
									<div class="col-md-6">
										<label>State:</label>						
										<select id="select_state" name="State"><option value="">Please Select</option></select>
									</div>
										<div class="col-md-6">
											<label>ZIP*:</label> <input type="text" name="ZIP" required>								
										</div>

									<div class="col-md-6">
										<label>Country*:</label>

										<select id="select_country" name="Country" required><option value="">Please Select</option></select>

										<!--<input type="text" name="Country">-->
									</div>

									<div class="col-md-12">
										<br/>
										<div class=" well well-sm">
											<span class="smaller"><em>* required field</em></span>
										</div>
									</div>

								</div>

								<input type="submit" class="submit-button" value="Submit"/>

							</form>

						</div>


					</div>
				</div>
			</div>

			<div id="footer" class="container-fluid">
				<div class="container">
					<img class="mobile-center" src="http://staging.vantagep.com/hennypenny/ms_5mistakes/exact_target/images/logo-hp.png" alt="HP" />
					<div class="clearfix"></div>
				</div>
			</div>

		</body>
		</html>